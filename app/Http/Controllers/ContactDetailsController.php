<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactDetails;

class ContactDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $contact = ContactDetails::findOrFail('1');
        // return view ('dashboard.contact.index',compact('contact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactDetails  $ContactDetails
     * @return \Illuminate\Http\Response
     */
    public function show(ContactDetails $ContactDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactDetails  $ContactDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactDetails $ContactDetails)
    {
        $contact = ContactDetails::findOrFail('1');
        return view ('dashboard.contact.edit',compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactDetails  $ContactDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactDetails $ContactDetails,$id)
    {
        $contact = ContactDetails::findOrFail($id);
        $contact->banner_text = $request->banner_text;
        $contact->address = $request->address;
        $contact->phone = $request->phone;
        $contact->mail = $request->mail;
        $contact->opening_hour = $request->opening_hour;
        $contact->map = $request->map;
        if(file_exists($request->file('contact_banner'))){
            $image = "banner".time().'.'.$request->file('contact_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('contact_banner')->move($location, $image);
            $contact->contact_banner = $image;
        }
        else{
            $contact->contact_banner = $contact->contact_banner;
        }        
        $contact->save();
        return redirect('/backoffice/contact');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactDetails  $ContactDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
