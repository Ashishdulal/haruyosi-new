<?php

namespace App\Http\Controllers;

use App\PageSetting;
use Illuminate\Http\Request;

class PageSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = PageSetting::findOrFail('1');
        return view('dashboard.page-setting.page',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function show(PageSetting $pageSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(PageSetting $pageSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PageSetting $pageSetting,$id)
    {
        $page = PageSetting::findOrFail($id);
        $page->site_title = $request->site_title;
        $page->tagline = $request->tagline;
        $page->site_url = $request->site_url;
        $page->email_address = $request->email_address;
        $page->footer_text = $request->footer_text;
        if($request->footer_visibility){
        $page->footer_visibility = $request->footer_visibility;
        }
        else{
        $page->footer_visibility = '0';
        }
        $page->copyright_text = $request->copyright_text;
        $page->meta_keywords_seo = $request->meta_keywords_seo;
        $page->meta_description_seo = $request->meta_description_seo;
        if(file_exists($request->file('site_logo'))){
            $image = $request->file('site_logo');
            $imageName =  "home".time().'.'.$request->file('site_logo')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $page->site_logo = $imageName;
        }
        else{
            $page->site_logo = $page->site_logo;
        }
        if(file_exists($request->file('site_favicon'))){
            $image = $request->file('site_favicon');
            $imageName =  "favicon".time().'.'.$request->file('site_favicon')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $page->site_favicon = $imageName;
        }
        else{
            $page->site_favicon = $page->site_favicon;
        }
        $page->save();
        return redirect()->back()->with('success','Page Settings Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PageSetting  $pageSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(PageSetting $pageSetting)
    {
        //
    }
}
