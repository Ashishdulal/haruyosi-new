<?php

namespace App\Http\Controllers;

use App\job;
use File;
use App\Jobs_detail;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = job::latest()->get();
        $jobBanner = Jobs_detail::findOrFail('1');
        return view('/dashboard/jobs/index', compact('jobs','jobBanner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('/dashboard/jobs/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bannerImage(Request $request,$id){
        $jobBanner = Jobs_detail::findOrFail($id);
        if(file_exists($request->file('jobs_banner'))){
            $image = "banner".time().'.'.$request->file('jobs_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('jobs_banner')->move($location, $image);
            $jobBanner->jobs_banner = $image;
        }
        else{
            $jobBanner->jobs_banner = $jobBanner->jobs_banner ;
        }
        if(file_exists($request->file('service_banner'))){
            $image = "banner".time().'.'.$request->file('service_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('service_banner')->move($location, $image);
            $jobBanner->service_banner = $image;
        }
        else{
            $jobBanner->service_banner = $jobBanner->service_banner ;
        }
        if(file_exists($request->file('portfolio_banner'))){
            $image = "banner".time().'.'.$request->file('portfolio_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('portfolio_banner')->move($location, $image);
            $jobBanner->portfolio_banner = $image;
        }
        else{
            $jobBanner->portfolio_banner = $jobBanner->portfolio_banner ;
        }
       if(file_exists($request->file('blog_banner'))){
            $image = "banner".time().'.'.$request->file('blog_banner')->getclientOriginalName();
            $location = public_path('images/banner');
            $request->file('blog_banner')->move($location, $image);
            $jobBanner->blog_banner = $image;
        }
        else{
            $jobBanner->blog_banner = $jobBanner->blog_banner ;
        }
        if($request->jobs_detail){
            $jobBanner->jobs_detail = $request->jobs_detail;
        }
        else{
            $jobBanner->jobs_detail = $jobBanner->jobs_detail ;
        }
        $jobBanner->save();
        return redirect()->back();

    }

    public function store(Request $request)
    {
        $jobs = new job();
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $jobs->name = $request->name;
        $jobs->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "jobs".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $jobs->image = $image;
        }
        else{
            $jobs->image = 'default-thumbnail.png';
        }        
        $jobs->save();
        return redirect('/backoffice/careers');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(job $job)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(job $job, $id)
    {
        $jobs = job::findOrFail($id);
        return view ('dashboard.jobs.edit',compact('jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, job $job, $id)
    {
        $jobs = job::findOrFail($id);
         $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $jobs->name = $request->name;
        $jobs->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "jobs".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads');
            $request->file('image')->move($location, $image);
            $jobs->image = $image;
        }
        else{
            $jobs->image = $jobs->image;
        }        
        $jobs->save();
        return redirect('/backoffice/careers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobs = job::findOrFail($id);
        $image_path = public_path("uploads/".$jobs->image);
        if(file_exists($image_path)){
            //File::delete($image_path);
            File::delete( $image_path);
        }
        $jobs->delete();
        return redirect()->back();
    }
}
