<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\staff;
use App\Service;
use App\Testimonial;
use App\homePage;
use App\Slider;
use App\Experience;
use App\Jobs_detail;
use App\PageSetting;
use App\job;
use App\Blog;
use App\about_us_page;
use App\Gallerycategory;
use App\Blogcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ContactDetails;
use App\Menu;

class MainPageController extends Controller
{
    public function index(){
        $pageSetting = PageSetting::findOrFail('1');
        $gallerycategories = Gallerycategory::latest()->get();
        $sliders = Slider::latest()->get();
        $testimonials = Testimonial::latest()->get();
        $services = Service::all();
        $homepages = homePage::findOrFail('1');
        $gallery = Gallery::latest()->get();
        $experiences = experience::all();
        $staffs = Staff::all();
    	return view ('new', compact('sliders','staffs','testimonials','services','gallery','gallerycategories','experiences','homepages','pageSetting'));
    }
    public function landingPage(){
        $pageSetting = PageSetting::findOrFail('1');
        $contact = ContactDetails::findOrFail('1');
        $homepages = homePage::findOrFail('1');
        $about = about_us_page::findOrFail('1');
        $testimonials = Testimonial::latest()->get();
        $staffs = Staff::all();
        $sliders = Slider::latest()->get();
        return view ('landing-page',compact('pageSetting','sliders','contact','homepages','about','testimonials','staffs'));
    }
    public function aboutPage(){
        $staffs = Staff::latest()->get();
        $pageSetting = PageSetting::findOrFail('1');
        $staffid = Staff::all();
        $about = about_us_page::findOrFail('1');
        $primarymenus = Menu::where('parent_id', null)->orderBy('order', 'ASC')->get();
    	return view ('newabout',compact('staffs','staffid','about','pageSetting'));
    }
    public function servicePage(){
        $pageSetting = PageSetting::findOrFail('1');
        $homepages = homePage::findOrFail('1');
        $serviceBanner = Jobs_detail::findOrFail('1');
        $testimonials = Testimonial::latest()->get();
        $services = Service::latest()->get();
        return view ('services',compact('services','testimonials','homepages','serviceBanner','pageSetting'));
    }

    public function  galleryPage()
    {
        $pageSetting = PageSetting::findOrFail('1');
    	$gallery = Gallery::latest()->get();
        $portfolioBanner = Jobs_detail::findOrFail('1');
        $homepages = homePage::findOrFail('1');
        $gallerycategories = Gallerycategory::latest()->get();
    	return view ('newgallery', compact('gallery','gallerycategories','homepages','portfolioBanner','pageSetting'));
    }
    public function  blogIndex()
    {
        $pageSetting = PageSetting::findOrFail('1');
        $blogs = Blog::latest()->paginate(4);
        $blogBanner = Jobs_detail::findOrFail('1');
        $gallery = Gallery::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        return view ('newblog', compact('blogs','blogcategories','gallery','blogBanner','pageSetting'));
    }
    public function  blogCategoryPage($id)
    {
        $pageSetting = PageSetting::findOrFail('1');
        $blogCatName = Blogcategory::findOrFail($id)->title;
        $blogBanner = Jobs_detail::findOrFail('1');
        $gallery = Gallery::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        $allBlogs = Blog::all();
        $blogs = Blog::where('cat_id',$id)->paginate();
        return view ('newblog-category',compact('blogs','blogcategories', 'id', 'blogCatName','gallery','blogBanner','pageSetting','allBlogs'));
    }
    public function blogDetail($id)
    {
        $pageSetting = PageSetting::findOrFail('1');
        $blogs = Blog::findOrFail($id);
        $blogBanner = Jobs_detail::findOrFail('1');
        $gallery = Gallery::latest()->get();
        $blogcategories = Blogcategory::latest()->get();
        $allblogs = Blog::latest()->paginate();
        return view ('blog-post',compact('blogs','blogcategories','allblogs','gallery','id','blogBanner','pageSetting'));
    }
    public function contact(){
        $pageSetting = PageSetting::findOrFail('1');
        $contact = ContactDetails::findOrFail('1');
        return view ('contacts',compact('contact','pageSetting'));
    }
    public function trainingPage(){
        return view ('training');
    }
    public function jobsIndex()
    {
        $pageSetting = PageSetting::findOrFail('1');
        $jobs = job::latest()->paginate(6);
        $jobBanner = Jobs_detail::findOrFail('1');
        $alljobs = job::all();
      return view('jobs',compact('jobs','alljobs','jobBanner','pageSetting'));
    }
    public function jobsDetail(job $job,$id)
    {
      // $jobs = job::findOrFail($id);
      // $alljobs = $job::latest()->get();
      // return view ('jobs-detail',compact('jobs','alljobs'));
    }
}
