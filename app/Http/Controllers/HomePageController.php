<?php

namespace App\Http\Controllers;

use App\homePage;
use Illuminate\Http\Request;

class HomePageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $homepages  = homePage::all();
        // return view ('dashboard.homepage.index', compact('homepages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\homePage  $homePage
     * @return \Illuminate\Http\Response
     */
    public function show(homePage $homePage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\homePage  $homePage
     * @return \Illuminate\Http\Response
     */
    public function edit(homePage $homePage)
    {
        $homepages  = homePage::findOrFail('1');
        return view ('dashboard.homepage.edit', compact('homepages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\homePage  $homePage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, homePage $homePage,$id)
    {
        $request->validate([
            'lower_body_image1' => 'mimetypes:video/mp4,video/avi,video/mpeg,video/mkv,qt | max:500000 ',
        ]);
        $homepages  = homePage::findOrFail($id);
        $homepages->phone_number = $request->phone_number;
        $homepages->opening_hours = $request->opening_hours;
        $homepages->Social_icon_fb = $request->Social_icon_fb;
        $homepages->Social_icon_insta = $request->Social_icon_insta;
        $homepages->Social_icon_twitter = $request->Social_icon_twitter;
        $homepages->Social_icon_linkedin = $request->Social_icon_linkedin;
        $homepages->upper_body_content = $request->upper_body_content;
        $homepages->service_body_content = $request->service_body_content;
        $homepages->staff_body_content = $request->staff_body_content;
        $homepages->lower_body_content = $request->lower_body_content;
        $homepages->schedule_content = $request->schedule_content;
        $homepages->why_us_content = $request->why_us_content;
        $homepages->portfolio_content = $request->portfolio_content;
        $homepages->logo_image = $request->logo_image;
        if(file_exists($request->file('lower_body_image1'))){
            $file = "homepages".time().'.'.$request->file('lower_body_image1')->getclientOriginalExtension();
            $location = public_path('uploads/homepage');
            $request->file('lower_body_image1')->move($location, $file);
            $homepages->lower_body_image1 = $file;
        }
        else{
            $homepages->lower_body_image1 =  $homepages->lower_body_image1;
        }
        if(file_exists($request->file('contact_body_content'))){
            $image = $request->file('contact_body_content');
            $imageName =  "why_schedule_image".time().'.'.$request->file('contact_body_content')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $homepages->contact_body_content = $imageName;
        }
        else{
            $homepages->contact_body_content = $homepages->contact_body_content;
        }
        if(file_exists($request->file('upper_body_image1'))){
            $image = $request->file('upper_body_image1');
            $iimageName =  "homepage_upper1".time().'.'.$request->file('upper_body_image1')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$iimageName);
            $homepages->upper_body_image1 = $iimageName;
        }
        else{
            $homepages->upper_body_image1 = $homepages->upper_body_image1;
        }
        if(file_exists($request->file('upper_body_image2'))){
            $image = $request->file('upper_body_image2');
            $imageName =  "homepage_upper2".time().'.'.$request->file('upper_body_image2')->getClientOriginalName();
            $image->move(public_path('uploads/homepage'),$imageName);
            $homepages->upper_body_image2 = $imageName;
        }
        else{
            $homepages->upper_body_image2 = $homepages->upper_body_image2;
        }
        $homepages->save();

        return redirect('/backoffice/homepage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\homePage  $homePage
     * @return \Illuminate\Http\Response
     */
    public function destroy(homePage $homePage)
    {
        //
    }
}