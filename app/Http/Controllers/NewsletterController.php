<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Newsletter;
use App\Blog;
use App\Blogcategory;

class NewsletterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newsletterIndex()
    {
        $newsletters = Newsletter::all();
        return view ('dashboard.newsletter.index', compact('newsletters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newsletterDestroy($id)
    {
        $newsletters = Newsletter::findOrFail($id)->delete();
        return redirect()->back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::latest()->get();
        $blogcategories = Blogcategory::all();
        $newsletters = Newsletter::all();
        return view ('dashboard.newsletter.create', compact('newsletters','blogcategories','blogs'));
    }
}
