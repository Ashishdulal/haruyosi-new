<?php

namespace App\Http\Controllers;

use App\User;
use File;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;

class UserPasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = User::all();
        return view ('dashboard.users.index',compact('users'));
    }
    public function create()
    {
        return view ('dashboard.users.create');
    }
    public function edit($id)
    {
        $users = User::findOrFail($id);
        return view ('dashboard.users.edit',compact('users'));
    }
    public function store(Request $request)
    {
        $users = new User();
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        return User::create([
            'name' => $users['name'],
            'email' => $users['email'],
            'image' => 'admin-image.png',
            'password' => Hash::make($users['password']),
        ])->with('success','User Profile Changed Successfully');
        // return redirect('/backoffice/users');
    }
    public function imageUpdate(Request $request,$id){
        $users = User::findOrFail($id);
        if(file_exists($request->file('image'))){
            $image = "user".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('images/admin');
            $request->file('image')->move($location, $image);
            $users->image = $image;
        }
        else{
            $users->image = $users->image;
        }
        $users->save();
        return redirect('/backoffice/users')->with('success','User Profile Changed Successfully');
    }
    public function update(Request $request,$id)
    {
        $users = User::findOrFail($id);
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = $request->password->Hash::make($data['password']);
        if(file_exists($request->file('image'))){
            $image = "user".time().'.'.$request->file('image')->getclientOriginalName();
            $location = public_path('images/admin');
            $request->file('image')->move($location, $image);
            $users->image = $image;
        }
        else{
            $users->image = $users->image;
        }
        $users->save();
        return redirect('/backoffice/users')->with('success','User Profile Changed Successfully');
    }
    public function destroy(Request $request,$id)
    {
        $users = User::findOrFail($id);
        $image_path = public_path("images/admin/".$users->image);
        if(file_exists($image_path)) {
            if ($users->image != 'admin-image.png'){
                //File::delete($image_path);
                File::delete($image_path);
        }
        }
        $users->delete();
        return redirect()->back()->with('success','User Has Been Deleted!');
    }
}
