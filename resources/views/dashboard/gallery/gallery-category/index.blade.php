@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/gallery">Portfolio</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Portfolio Categories</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12">
				<a href="/backoffice/gallery-category/create" class="btn btn-primary dashboard-button">Add New Portfolio Category</a>
				<!-- DATA TABLE-->
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<table class="table table-hover">
							<tr>
								<th>S. NO.</th>
								<th>Portfolio Category</th>
								<th>Action</th>
							</tr>

							@foreach ($gallerycategories as $gallerycategory)
							<tr>
								<td>{{$gallerycategory->id}}</td>
								<td>{{$gallerycategory->title}}</td>
								<td class="make_btn_straight"><a href="/backoffice/gallery-category/edit/{{$gallerycategory->id}}"> <button class="btn btn-info make-btn">Edit</button></a> |
									<form method="delete" action="{{route('delete.gallery',$gallerycategory->id)}}"> 

										{{csrf_field()}}

										{{method_field('DELETE')}}
										<div class="field"><button class="btn btn-danger" onclick="makeWarning(event)">Delete</button></div>
									</form>

								</td>
							</tr>
							@endforeach
						</table>
					</div>
					<div class="card-footer">
						
					</div>
				</div>
			</div>



		</div><!--/.col-->
		<script type="text/javascript">
			function makeWarning(evt){
				let result = confirm("Are you sure to Delete?");
				if(! result){
					evt.stopPropagation();
					evt.preventDefault();	
				}
			}
		</script>
		@endsection