@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/backofficepage">Homepage Contents</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12">
				<a style="margin-bottom: 10px; float: right;" href="{{route('homepage.edit', 1)}}"><button class="btn btn-primary make-btn">Edit HomePage Contents</button></a>
				<!-- <a style="margin-bottom: 10px;" href="/backoffice/backofficepage/create" class="btn btn-primary">Add New Service</a> -->
				<!-- DATA TABLE-->
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<thead>
							<tr>
								<!-- 						<th>Id</th> -->
								<th>Logo Image</th>
								<th>Phone Number</th>
								<th>Opening Hours</th>
								<th>Social Media</th>
								<th>Upper Body Image</th>
								<th>Upper Body Content</th>
								<th>Service Body Content</th>
								<th>Staff Body Content</th>
								<th>Contact Body Content</th>
								<th>Lower Body Video</th>
								<th>Lower Body Content</th>
								<th>Schedule Body Content</th>
								<th>Why Us Body Content</th>
								<th>Portfolio Body Content</th>
							</tr>
						</thead>
						<tbody>
							@foreach($homepages as $homepage)
							<tr>
								<!-- <td>{{ $loop->iteration }}</td> -->
								<td class="process"><img src="/uploads/backofficepage/{{$homepage->logo_image}}"></td>
								<td>{{$homepage->phone_number}}</td>
								<td>{{$homepage->opening_hours}}</td>
								<td>
									<ul>
										<li>Facebook: {{$homepage->Social_icon_fb}}</li>
										<li>Instagram: {{$homepage->Social_icon_insta}}</li>
										<li>Twitter: {{$homepage->Social_icon_twitter}}</li>
										<li>Linkedin: {{$homepage->Social_icon_linkedin}}</li>
									</ul>
								</td>
								<td class="process">
									<img src="/uploads/backofficepage/{{$homepage->upper_body_image1}}"><br>
									<img src="/uploads/backofficepage/{{$homepage->upper_body_image2}}">
								</td>
								<td><?php echo ($homepage->upper_body_content)?></td>
								<td><?php echo ($homepage->service_body_content)?></td>
								<td><?php echo ($homepage->staff_body_content)?></td>
								<td><?php echo ($homepage->contact_body_content)?></td>
								<td class="">
									<iframe src="{{$homepage->lower_body_image1}}" volume="0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</td>
								<td><?php echo ($homepage->lower_body_content)?></td>
								<td><?php echo ($homepage->schedule_content)?></td>
								<td><?php echo ($homepage->why_us_content)?></td>
								<td><?php echo ($homepage->portfolio_content)?></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
				<a style="margin: 10px;" href="{{route('homepage.edit', 1)}}"><button class="btn btn-primary make-btn">Edit HomePage Contents</button></a>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection