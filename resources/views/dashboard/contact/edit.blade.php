@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Dashboard</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Contact Content</li>
				</ul>
			</div>
		</div>
		@if($errors->any())
		@foreach($errors->all() as $error)
		<ul>
			<li>{{$error}}</li>
		</ul>
		@endforeach
		@endif
		<div class="card-body card-block">
			<span class="au-breadcrumb-span note-space">[Note: <a href="#help_image">Click For Help</a>]</span>
			<form action="/backoffice/contact/edit/{{$contact->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
				@csrf
				<button style="float: right;" type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="contact_banner" class=" form-control-label">Banner Image (1)</label><br>
						<span class="au-breadcrumb-span">[ Note: Please upload the image size [1920*305] & should be less than 100kb ]</span>
					</div>
					<div class="col-12 col-md-9 process">
						<input onchange="readURL(this);" type="file" id="contact_banner" accept="image/png, image/jpg, image/jpeg" name="contact_banner" class="form-control-file"><br>
						<img id="blah" src="/images/banner/{{$contact->contact_banner}}" alt="{{$contact->name}}">
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="banner_text" class=" form-control-label">Contact Title Description (2)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="banner_text"  rows="9"  class="form-control ckeditor">{{$contact->banner_text}}</textarea>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="address" class=" form-control-label">Address (3)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="text" name="address"  rows="9"  class="form-control" value="{{$contact->address}}" placeholder="Address"><br>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="address" class=" form-control-label">Phone Number (4)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="text" name="phone"  rows="9"  class="form-control" value="{{$contact->phone}}" placeholder="phone"><br>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="address" class=" form-control-label">Mail Address (5)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="text" name="mail"  rows="9"  class="form-control" value="{{$contact->mail}}" placeholder="mail"><br>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="address" class=" form-control-label">Map Iframe (6)</label>
					</div>
					<div class="col-12 col-md-9">
						<input type="text" name="map"  rows="9"  class="form-control" value="{{$contact->map}}" placeholder="map Iframe"><br>
					</div>
				</div>
				<div class="row form-group">
					<div class="col col-md-3">
						<label for="opening_hour" class=" form-control-label">Opening Hours (7)</label>
					</div>
					<div class="col-12 col-md-9">
						<textarea name="opening_hour"  rows="9"  class="form-control ckeditor">{{$contact->opening_hour}}</textarea>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Update
				</button>
			</form>
		</div>
		<div id="help_image" class="card-footer">
			<span class="au-breadcrumb-span">[The below Numbers in the image denotes upper number for changes.]  </span><br><br
			<div class="row">
				<!-- <div class="col-md-6"> -->
					<div class="col-md-12 help-image">
						<img src="/images/contact-page.jpg" alt="Image">
					</div>
				</div>
			</div>
		</div>
	</div>



</div><!--/.col-->

@endsection