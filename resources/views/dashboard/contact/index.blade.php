@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Dashboard</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/contact">Contact Contents</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12">
				<a style="margin-bottom: 10px; float: right;" href="{{route('contact.edit', 1)}}"><button class="btn btn-primary make-btn">Edit Contact Contents</button></a>
				<!-- DATA TABLE-->
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<thead>
							<tr>
								<th>Banner Image</th>
								<th>Banner Text</th>
								<th>Address</th>
								<th>Phone</th>
								<th>Mail</th>
								<th>Opening Hours</th>
								<th>Map</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="process"><img src="/images/banner/{{$contact->contact_banner}}"></td>
								<td><?php echo ($contact->banner_text)?></td>
								<td><?php echo ($contact->address)?></td>
								<td>{{$contact->phone}}</td>
								<td>{{$contact->mail}}</td>
								<td><?php echo ($contact->opening_hour)?></td>
								<td><?php echo ($contact->map)?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
				<a style="margin: 10px;" href="{{route('contact.edit', 1)}}"><button class="btn btn-primary make-btn">Edit AboutUs Contents</button></a>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection