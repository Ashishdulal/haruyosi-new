@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">
						<a href="/backoffice/experience">expertise</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row m-t-30">
			<div class="col-md-12">
				<a href="/backoffice/experience/create" class="btn btn-primary dashboard-button">Add New expertise</a>
				<!-- DATA TABLE-->
				<div class="table-responsive m-b-40">
					<table class="table table-borderless table-data3">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Image</th>
								<th width="300px">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($experiences as $experience)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{$experience->name}}</td>
								<td><img style="max-width: 50%;" src="/uploads/{{$experience->image}}"></td>
								<td><a href="{{route('experience.edit', $experience->id)}}"><button class="btn btn-primary make-btn">Edit</button></a>
									<form method="post" action="{{route('experience.delete',$experience->id)}}">
										@csrf
										{{ method_field('DELETE') }}
										<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
</script>

@endsection