@extends('layouts.dashboard.app')

@section('content')
<!-- PAGE CONTAINER-->
<div class="col-lg-12">
	<div class="card">
		<div class="card-header">
			<div class="au-breadcrumb-left">
				<span class="au-breadcrumb-span">You are here:</span>
				<ul class="list-unstyled list-inline au-breadcrumb__list">
					<li class="list-inline-item">
						<a href="/backoffice">Home</a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item">
						<a href="/backoffice/menus">Menu </a>
					</li>
					<li class="list-inline-item seprate">
						<span>/</span>
					</li>
					<li class="list-inline-item active">Edit</li>
				</ul>
			</div>
		</div>
		<div class="card-body card-block">
		@if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
			<form  method="POST" action={{route('update.menu',$menus->id)}}  class="ng-untouched ng-pristine ng-valid">

				{{csrf_field()}}

				<label for="label" >Menu Label</label>
				<div  class="form-group">
					<input type="text" name="label" class="form-control {{ $errors->has('label') ? 'is-danger': '' }}" value="{{ $menus->label }}" placeholder="Enter the menu label">
				</div>
				<label for="url" >Menu URL</label>
				<div  class="form-group">
					<input type="text" name="url" class="form-control {{ $errors->has('url') ? 'is-danger': '' }}" value="{{ $menus->url }}" placeholder="Enter the menu url">
				</div>
				<label for="order" >Menu Order</label>
				<div  class="form-group">
					<input type="number" min="1" name="order" class="form-control {{ $errors->has('order') ? 'is-danger': '' }}" value="{{ $menus->order }}" placeholder="Enter the menu order">
				</div>
				<label for="parent_id" >Menu Label</label>
				<div class="form-group">
						<select name="parent_id" class="form-control">
							@foreach($alllmenu as $mymenu)
							@if($mymenu->id == $menus->parent_id)
							<option value="{{$mymenu->id}}">{{$mymenu->label}}</option>
							@endif
							@endforeach
							<option value="">---</option>
							@foreach($alllmenu as $mymenu)
							@if($mymenu->id != $menus->parent_id)
							<option value="{{$mymenu->id}}">{{$mymenu->label}}</option>
							@endif
							@endforeach
						</select>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-dot-circle-o"></i> Submit
				</button>
				<button type="reset" class="btn btn-danger btn-sm">
					<i class="fa fa-ban"></i> Reset
				</button>
			</form>
		</div>
		<div class="card-footer">
			
		</div>
	</div>
</div>



</div><!--/.col-->

@endsection