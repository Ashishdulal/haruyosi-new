 @extends('layouts.new.app', ['title' => 'About'],['discription'=> ($pageSetting->tagline)],['site_url'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" style="background-image: url(uploads/about-us/{{$about->banner_image}}); background-size: cover;">
  <div class="container">
    <h1 class="page-title">About Us
      <!-- <?php
    // Program to display URL of current page. 
 $actual_link =$_SERVER['PHP_SELF'];
echo ($actual_link);
?> -->
</h1>
</div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">About Us</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50 align-items-lg-center justify-content-xl-between">
      <div class="col-lg-6 text-justify">
        <div class="block-xs">
        </div>
        <?php echo ($about->who_are_we)?>
      </div>
      <div class="col-lg-6">
        <div class="box-images box-images-variant-3">
          <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -20,   &quot;smoothness&quot;: 30 }"><img src="/uploads/about-us/{{$about->who_are_we_image1}}" alt="" width="470" height="282"/>
          </div>
          <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="/uploads/about-us/{{$about->who_are_we_image2}}" alt="" width="470" height="282"/>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 <section class="section-xl video-bg-overlay bg-vide" data-vide-bg="mp4: images/bg-video-test, poster: images/bg-video" data-vide-options="posterType: jpg, position: 0% 50%"><div style="position: absolute; z-index: 0; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 0% 50%; background-image: none;"><video autoplay="" loop="" muted="" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 0%; transform: translate(0%, -50%); visibility: visible; opacity: 1; width: 1888px; height: auto;" __idm_id__="683964417"><source src="/uploads/about-us/{{$about->video}}" type="video/mp4"></video></div>
        <div class="section-sm context-dark text-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-10">
                <div class="pb-5">
                  <?php echo ($about->video_title)?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<section id="haruyosi_about" class="section section-md bg-default text-center">
  <div class="container">
    <h2>Our Professional Team</h2>
    <div class="divider-lg"></div>
    <p class="block-lg">{{$about->team_description}} </p>
    <div class="row row-30">
      <div class="col-12">
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-creative" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          <div class="team-minimal team-minimal-with-shadow">
            <figure><a href="" data-toggle="modal" data-target="#myModal{{$staff->id}}"><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></a></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title"><a href="#" data-toggle="modal" data-target="#myModal{{$staff->id}}">{{$staff->name}} </a></h4>
              <p>{{$staff->designation}}</p>
            </div>
          </div>
          @endforeach

        </div>

        @foreach($staffs as $staff)
        <!-- Modal -->
        <div class="modal fade" id="myModal{{$staff->id}}" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <section class="section bg-default">
                  <div class="container">
                    <div class="row row-50">
                      <div class="col-lg-6 col-xl-4 text-center">
                        <div class="row row-50">
                          <div class="col-12"><img class="img-shadow" src="/uploads/{{$staff->image}}" alt="" width="370" height="370"/>
                          </div>
                          <div style="margin-bottom: 20px;" class="col-12"><a style="padding: 8px 20px;" class="button button-primary" href="mailto:{{$staff->mail}}">Mail Me</a></div>
                          <!--                           <div class="col-12"><a style="padding: 10px 15px;" class="button button-primary" href="/contacts">Make an appointment</a></div> -->
                          <div class="col-12">
                            <ul class="list-inline social-list">
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-facebook" target="_blank" href="{{$staff->facebook_link}}"></a></li>
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-twitter" target="_blank" href="{{$staff->twitter_link}}"></a></li>
                              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-instagram" target="_blank" href="{{$staff->instagram_link}}"></a></li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6 col-xl-8">
                        <div class="box-team-info">
                          <div class="box-team-info-header">
                            <h3>{{$staff->name}}</h3>
                            <p>{{$staff->designation}}</p>
                            <div class="divider-lg"></div>
                          </div>
                          <div style="text-align: justify;"><?php echo ($staff->description)?></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
              <div style="padding: 30px;" class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
      <!-- <div class="col-12"><a class="button button-default-outline" href="/our-team">View all team</a></div> -->
    </div>
  </div>
</section>
<section class="section-transform-bottom">
  <div class="container-fluid section-md bg-primary context-dark">
    <div style="margin-right: 0px;" class="row justify-content-center row-50">
      <div class="col-sm-10 text-center">
        <h2>Subscribe to Our Newsletter</h2>
        <div class="divider-lg"></div>
      </div>
      <div class="col-sm-10 col-lg-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}<br></li>
            @endforeach
          </ul>
        </div>
        @endif
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif
        <!-- RD Mailform-->
        <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
          @csrf
          <div class="form-wrap">
            <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
            <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
          </div>
          <div class="form-button1">
            <button class="button button-primary" type="submit">Subscribe</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
@endsection