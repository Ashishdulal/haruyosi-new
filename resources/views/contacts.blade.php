 @extends('layouts.new.app', ['title' => 'Contact'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/banner/{{$contact->contact_banner}}); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Contacts</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Contacts</li>
    </ul>
  </div>
</section>
<!-- Mailform-->
<section class="section section-md">
  <div class="container container-responsive">
    <div class="row row-50">
      <div class="col-lg-8">
        <h2>Contact us</h2>
        <div class="divider-lg"></div>
        <?php echo ($contact->banner_text)?>
        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}<br></li>
              @endforeach
            </ul>
          </div>
          @endif
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif
      <!-- RD Mailform-->
      <form class="text-left" action="{{url('sendemail/send')}}" method="post" >
        @csrf
        <div class="row row-15">
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-name" type="text" name="name" placeholder="First name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-sec-name" type="text" name="sec_name" placeholder="Last name" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-phone" type="text" placeholder="Phone" name="phone" required="">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="form-wrap">
              <input class="form-input" id="contact-email" type="email" name="email" placeholder="E-Mail" required="">
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-wrap">
              <input class="form-input" id="contact-subject" type="subject" placeholder="Subject" name="subject" required="">
            </div>
          </div>
          <div class="col-12">
            <div class="form-wrap">
              <!-- <label class="form-label" for="contact-message"></label> -->
              <textarea class="form-input" id="contact-message" placeholder="Message" name="message" required=""></textarea>
            </div>
          </div>
        </div>
        <div class="form-button group-sm text-left">
          <button class="button button-primary" type="submit">Send message</button>
        </div>
      </form>
    </div>
    <div class="col-lg-4">
      <ul class="contact-list">
        <li> 
          <p class="contact-list-title">Address</p>
          <div class="contact-list-content"><span class="icon mdi mdi-map-marker icon-primary"></span>
            <?php echo ($contact->address)?>
          </div>
        </li>
        <li>
          <p class="contact-list-title">Phone</p>
          <div class="contact-list-content"><span class="icon mdi mdi-phone icon-primary"></span><a href="tel:{{$contact->phone}}">{{$contact->phone}}</a></div>
        </li>
        <li>
          <p class="contact-list-title">E-mail</p>
          <div class="contact-list-content"><span class="icon mdi mdi-email-outline icon-primary"></span><a href="mailto:{{$contact->mail}}"><span>{{$contact->mail}}</span></a></div>
        </li>
        <li>
          <p class="contact-list-title">Opening Hours</p>
          <div class="contact-list-content"><span class="icon mdi mdi-clock icon-primary"></span>
            <?php echo ($contact->opening_hour)?>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>
</section>
<!-- Page Footer-->
<!-- Google map-->
<section class="section">
  <div class="google-map-container">
  <?php echo ($contact->map)?>
  </div>
</section>
@endsection