 @extends('layouts.new.app', ['title' => 'Services'],['discription'=> ($pageSetting->tagline)])

 @section('content')
 <section class="section-page-title" style="background-image: url(images/banner/{{$serviceBanner->service_banner}}); background-size: cover;">
  <div class="container">
    <h1 class="page-title">Services</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li class="active">Services</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container">
    <div class="row row-50 align-items-lg-center justify-content-xl-between">
      <div class="col-lg-6 text-justify section-text">
        <div class="block-xs">
          <?php echo ($homepages->upper_body_content)?>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="box-images box-images-modern">
          <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -10,   &quot;smoothness&quot;: 30 }"><img src="/uploads/homepage/{{$homepages->upper_body_image1}}" alt="" width="310" height="370"/>
          </div>
          <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="/uploads/homepage/{{$homepages->upper_body_image2}}" alt="" width="328" height="389"/>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>    
<section class="section section-lg bg-gray-100 text-center">
  <div class="container-fluid container-responsive">
    <h2>Our Services</h2>
    <div class="divider-lg"></div>
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-9">
        <p><?php echo ($homepages->service_body_content)?></p>
      </div>
    </div>
    <div class="row icon-modern-list no-gutters">
      @foreach($services as $service)
      <div class="col-sm-6 col-lg-4">
        <article class="box-icon-modern modern-variant-2">
          <div class="icon-modern">
            <span> <a data-toggle="modal" data-target="#services{{$service->id}}" href="#"> <img style="cursor:pointer;" aria-expanded="true" src="/uploads/{{$service->image}}"></a></span>
          </div>
          <h4 class="box-icon-modern-title"><a data-toggle="modal" data-target="#services{{$service->id}}" href="#">{{$service->name}}</a></h4>
<!--             <div class="divider"></div>
  <p><?php echo ($service->description)?></p> -->
</article>
</div>
<!-- Modal -->
<div class="modal fade" id="services{{$service->id}}" role="dialog">
  <div class="modal-dialog modal-lg">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Our Services</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <article class="modern-variant-2">
        <div class="icon-modern">
          <span><img style="max-width: 50%;" aria-expanded="true" aria-controls="services{{$service->id}}" style="" src="/uploads/{{$service->image}}"></span>
        </div>
        <div style="text-align: left;margin-top: 10px;">
          <h4 style="margin-bottom: 10px;" class="box-icon-modern-title">{{$service->name}}</h4>
          <div class="divider"></div>
          <p><?php echo ($service->description)?></p>
        </div>
      </article>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
  
</div>
</div>
@endforeach
</div>
</div>
</section>
<section class="section section-xl bg-default schedule-part">
  <div class="container container-responsive">
    <div class="row no-gutters pricing-box-modern justify-content-lg-end">
      <div class="col-sm-6 col-lg-4">
        <div class="pricing-box-inner box-left">
          <?php echo ($homepages->schedule_content)?></p>

          <a class="button-link button-link-icon" href="#" data-toggle="modal" data-target="#myModal">make an appointment  <span class="icon fa-arrow-right icon-primary"></span></a>
        </div>
      </div>
      <div class="d-none d-lg-block col-lg-4 img-wrap"><img src="/uploads/homepage/{{$homepages->contact_body_content}}" alt="" width="498" height="688"/>
      </div>
      <div class="col-sm-6 col-lg-4 bg-primary">
        <div class="pricing-box-inner context-dark box-right">
          <?php echo ($homepages->why_us_content)?></p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section parallax-container" data-parallax-img="images/parallax-7-1920x1020.jpg">
  <div class="parallax-content section-lg text-center ">
    <div class="container container-responsive"> 
      <h2>Testimonials</h2>
      <div class="divider-lg"></div>
      <!-- Owl Carousel-->
      <div class="owl-carousel" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
        @foreach($testimonials as $testimonial)
        <div class="quote-corporate quote-corporate-center-img">
          <div class="quote-header">
            <h4>{{$testimonial->name}}</h4>
            <p class="big">Client</p>
          </div>
          <div class="quote-body">
            <div class="quote-text">
              <p><?php echo ($testimonial->description)?></p>
            </div>
            <svg class="quote-body-mark" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="66px" height="49px" viewbox="0 0 66 49" enable-background="new 0 0 66 49" xml:space="preserve">
              <g></g>
              <path d="M36.903,49v-3.098c9.203-5.315,14.885-12.055,17.042-20.222c-2.335,1.524-4.459,2.288-6.37,2.288                      c-3.186,0-5.875-1.29-8.071-3.876c-2.194-2.583-3.293-5.74-3.293-9.479c0-4.133,1.443-7.605,4.327-10.407                       C43.425,1.405,46.973,0,51.185,0c4.213,0,7.735,1.784,10.566,5.352C64.585,8.919,66,13.359,66,18.669                       c0,7.482-2.85,14.183-8.549,20.112C51.751,44.706,44.902,48.112,36.903,49z M0.69,49v-3.098                        c9.205-5.315,14.887-12.055,17.044-20.222c-2.335,1.524-4.478,2.288-6.423,2.288c-3.152,0-5.823-1.29-8.02-3.876                        C1.096,21.51,0,18.353,0,14.614c0-4.133,1.434-7.605,4.301-10.407C7.168,1.405,10.709,0,14.92,0c4.247,0,7.778,1.784,10.592,5.352                       c2.814,3.567,4.223,8.007,4.223,13.317c0,7.482-2.843,14.183-8.524,20.112C15.53,44.706,8.69,48.112,0.69,49z"></path>
            </svg>
          </div>
          <div class="quote-image"><img src="/uploads/{{$testimonial->image}}" alt="" width="90" height="90"/>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>
<section class="section-transform-bottom">
  <div class="container-fluid section-md bg-primary context-dark">
    <div style="margin-right: 0px;" class="row justify-content-center row-50">
      <div class="col-sm-10 text-center">
        <h2>Subscribe to Our Newsletter</h2>
        <div class="divider-lg"></div>
      </div>
      <div class="col-sm-10 col-lg-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}<br></li>
            @endforeach
          </ul>
        </div>
        @endif
        @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif
        <!-- RD Mailform-->
        <form class="rd-form-inline" method="post" action="{{url('/subscribe/send')}}">
          @csrf
          <div class="form-wrap">
            <input class="form-input" id="subscribe-form-0-email" type="email" name="email" required="" />
            <label class="form-label" for="subscribe-form-0-email">Your E-mail</label>
          </div>
          <div class="form-button1">
            <button class="button button-primary" type="submit">Subscribe</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Page Footer-->
@endsection