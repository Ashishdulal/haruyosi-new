<!DOCTYPE html>
<html lang="en">

<head>
    <?php 
    use App\PageSetting;
    
    $pageSetting = PageSetting::findOrFail('1');
    ?>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{$pageSetting->meta_description_seo}}">
    <meta name="keywords" content="{{$pageSetting->meta_keywords_seo}}">

    
    <!-- Title Page-->
    <title>Dashboard | {{$pageSetting->site_title}}</title>

    <!-- Favicon-->
    <link rel="icon" href="/uploads/homepage/{{$pageSetting->site_favicon}}" type="image/x-icon">

    <!-- Fontfaces CSS-->
    <link href="{{asset('css/font-face.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-4.7/css/font-awesome.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/font-awesome-5/css/fontawesome-all.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/mdi-font/css/material-design-iconic-font.min.css')}}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{{asset('vendor/bootstrap-4.1/bootstrap.min.css')}}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{asset('vendor/animsition/animsition.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/wow/animate.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/css-hamburgers/hamburgers.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/slick/slick.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/select2/select2.min.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.css')}}" rel="stylesheet" media="all">
    <link href="{{asset('vendor/vector-map/jqvmap.min.css')}}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{asset('css/theme.css')}}" rel="stylesheet" media="all">
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar2 js-right-sidebar d-block ">
            <div class="logo">
                <a style="text-align: center;" href="/">
                    <img class="dashboard-logo" src="/uploads/homepage/{{$pageSetting->site_logo}}" alt="go to homepage">
                    <!-- <h2 class="name">{{$pageSetting->site_title}}</h2> -->
                </a>
            </div>
            <div class="menu-sidebar2__content js-scrollbar1">
                <div style="padding: 5px 0;"></div>
<!--             <div class="account2">
                <div class="image img-cir img-120">
                    <img src="{{asset('images/admin-image.png')}}" alt="Administrator" />
                </div>
                <h4 class="name">Administrator</h4>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                {{ __('Sign out') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div> -->
        <nav class="navbar-sidebar2">
            <ul class="list-unstyled navbar__list">
                <li>
                    <a href="/backoffice">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                    </li>
                    <li>
                        <a href="/backoffice/page-setting">
                            <i class="fa fa-cogs" aria-hidden="true"></i>Page Settings</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                             <i class="fas fa-align-justify"></i>Menu
                             <span class="arrow">
                                <i class="fas fa-angle-down"></i>
                            </span>
                        </a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="/backoffice/menus">
                                    <i class="fas fa-bars"></i>All Menu</a>
                                </li>
                                <li>
                                    <a href="/backoffice/homepage">
                                        <i class="fa fa-newspaper-o"></i>Homepage</a>
                                    </li>
                                    <li>
                                        <a href="/backoffice/about-us">
                                            <i class="fa fa-book" aria-hidden="true"></i>AboutUs page</a>
                                        </li>
                                        <li>
                                            <a class="js-arrow" href="#">
                                                <i class="fa fa-cogs" aria-hidden="true"></i>Services
                                                <span class="arrow">
                                                    <i class="fas fa-angle-down"></i>
                                                </span>
                                            </a>
                                            <ul class="list-unstyled js-sub-list">
                                                <li>
                                                    <a href="/backoffice/services">
                                                        <i class="fa fa-cog" aria-hidden="true"></i>All Services</a>
                                                    </li>
                                                    <li>
                                                        <a href="/backoffice/services/create">
                                                            <i class="fa fa-cog" aria-hidden="true"></i>Create Service</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="has-sub">
                                                    <a class="js-arrow" href="#">
                                                        <i class="fas fa-image"></i>Portfolio
                                                        <span class="arrow">
                                                            <i class="fas fa-angle-down"></i>
                                                        </span>
                                                    </a>
                                                    <ul class="list-unstyled js-sub-list">
                                                        <li>
                                                            <a href="/backoffice/gallery-category">
                                                                <i class="fas fa-plus-square"></i>Portfolio Category</a>
                                                            </li>
                                                            <li>
                                                                <a href="/backoffice/gallery">
                                                                 <i class="fas fa-images"></i>All Portfolio</a>
                                                             </li>
<!--                                 <li>
                                    <a href="/backoffice/gallery/create">
                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>Create Image</a>
                                    </li> -->
                                <!-- <li>
                                    <a href="/backoffice/gallery/createVideo">
                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>Create Video</a>
                                    </li> -->
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-copy"></i>News
                                    <span class="arrow">
                                        <i class="fas fa-angle-down"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled js-sub-list">
                                    <li>
                                        <a href="/backoffice/news-category">
                                         <i class="fas fa-plus-square"></i>News Category</a>
                                     </li>
                                     <li>
                                        <a href="/backoffice/news">
                                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>News Posts</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="/backoffice/careers">
                                        <i class="fa fa-users" aria-hidden="true"></i>Careers</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="has-sub">
                                <a class="js-arrow" href="#">
                                    <i class="fas fa-trophy"></i>Sections
                                    <span class="arrow">
                                        <i class="fas fa-angle-down"></i>
                                    </span>
                                </a>
                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                    <li>
                                        <a href="/backoffice/experience">
                                            <i class="fas fa-book"></i>Expertise</a>
                                        </li>
                                        <li class="has-sub">
                                            <a class="js-arrow" href="#">
                                                <i class="fas fa-image"></i>Slider
                                                <span class="arrow">
                                                    <i class="fas fa-angle-down"></i>
                                                </span>
                                            </a>
                                            <ul class="list-unstyled js-sub-list">
                                                <li>
                                                    <a href="/backoffice/sliders">
                                                     <i class="fas fa-images"></i>All Sliders</a>
                                                 </li>
                                                 <li>
                                                    <a href="/backoffice/slider/create">
                                                        <i class="fa fa-file-image-o" aria-hidden="true"></i>Create Slider</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="/backoffice/testimonials">
                                                    <i class="fa fa-quote-left" aria-hidden="true"></i>Testimonial</a>
                                                </li>
                                                <li>
                                                    <a href="/backoffice/staffs">
                                                        <i class="fa fa-users" aria-hidden="true"></i>Staffs</a>
                                                    </li>
                                                    <li>
                                                        <a href="/backoffice/contact">
                                                            <i class="fa fa-phone" aria-hidden="true"></i>Contact Page</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="has-sub">
                                                    <a class="js-arrow" href="#">
                                                     <i class="fas fa-clipboard-list"></i>Newsletter
                                                     <span class="arrow">
                                                        <i class="fas fa-angle-down"></i>
                                                    </span>
                                                </a>
                                                <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                    <li>
                                                        <a href="/backoffice/subscribers">
                                                            <i class="fa fa-users" aria-hidden="true"></i>All Subscribers</a>
                                                        </li>
                                                        <li>
                                                            <a href="/backoffice/newsletter">
                                                                <i class="fa fa-newspaper-o" aria-hidden="true"></i>Newsletter Post</a>
                                                            </li>
                                                        </ul>
                                                    </li>

                                                    <li>
                                                        <a href="/backoffice/users">
                                                            <i class="fa fa-users" aria-hidden="true"></i>User Management</a>
                                                        </li>
                                                        <li><a class="js-arrow open" href="#"></a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </aside>
                                        <!-- END MENU SIDEBAR-->
                                        <?php 
                                        use App\User;
                                        $id = Auth::user()->id;
                                        $currentuser = User::find($id);
                                        ?>
                                        <div class="page-container2">
                                            <!-- HEADER DESKTOP-->
                                            <header class="header-desktop2">
                                                <div class="section__content section__content--p30">
                                                    <div class="container-fluid">
                                                        <div class="header-wrap2 my-header-wrap">
                                                            <div class="logo d-block d-lg-none">
                                                                <a href="/">
                                                                    <img src="/images/kx-logo.png" alt="AbsoluteAesthetics" />
                                                                </a>
                                                            </div>
                                                            <div class="account-wrap">
                                                                <div class="account-item account-item--style2 clearfix js-item-menu">
                                                                    <div class="image">
                                                                        <img src="/images/admin/{{$currentuser->image}}" alt="Administrator">
                                                                    </div>
                                                                    <div class="content">
                                                                        <a class="js-acc-btn" href="#">{{$currentuser->name}}</a>
                                                                    </div>
                                                                    <div class="account-dropdown js-dropdown">
                                                                        <div class="info clearfix">
                                                                            <div class="image">
                                                                                <img src="/images/admin/{{$currentuser->image}}" alt="Administrator">
                                                                            </div>
                                                                            <div class="content">
                                                                                <h5 class="name">
                                                                                    {{$currentuser->name}}
                                                                                </h5>
                                                                                <span class="email">{{$currentuser->email}}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="account-dropdown__body">
                                                                            <div class="account-dropdown__item">
                                                                                <a href="/backoffice/users">
                                                                                    <i class="zmdi zmdi-account"></i>Account</a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="account-dropdown__footer">
                                                                                <a href="{{ route('logout') }}"
                                                                                onclick="event.preventDefault();
                                                                                document.getElementById('logout-form').submit();">
                                                                                <i class="zmdi zmdi-power"></i>{{ __('Sign out') }}
                                                                            </a>
                                                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                                @csrf
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="header-button-item  js-sidebar-btn responsive-menu-bar">
                                                                <i class="zmdi zmdi-menu"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </header>
                                            <section class="au-breadcrumb m-t-75 my-breadcrum mobile-low-pad">
                                            </section>


                                            @yield('content')

                                            <section>
                                                <div class="container-fluid">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="copyright">
                                                                <?php echo ($pageSetting->copyright_text)?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- END PAGE CONTAINER-->
                                        </div>
                                    </div>

                                    <!-- Jquery JS-->
                                    <script src="{{asset('vendor/jquery-3.2.1.min.js')}}"></script>
                                    <!-- Bootstrap JS-->
                                    <script src="{{asset('vendor/bootstrap-4.1/popper.min.js')}}"></script>
                                    <script src="{{asset('vendor/bootstrap-4.1/bootstrap.min.js')}}"></script>
                                    <!-- Vendor JS       -->
                                    <script src="{{asset('vendor/slick/slick.min.js')}}">
                                    </script>
                                    <script src="{{asset('vendor/wow/wow.min.js')}}"></script>
                                    <script src="{{asset('vendor/animsition/animsition.min.js')}}"></script>
                                    <script src="{{asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js')}}">
                                    </script>
                                    <script src="{{asset('vendor/counter-up/jquery.waypoints.min.js')}}"></script>
                                    <script src="{{asset('vendor/counter-up/jquery.counterup.min.js')}}">
                                    </script>
                                    <script src="{{asset('vendor/circle-progress/circle-progress.min.js')}}"></script>
                                    <script src="{{asset('vendor/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
                                    <script src="{{asset('vendor/chartjs/Chart.bundle.min.js')}}"></script>
                                    <script src="{{asset('vendor/select2/select2.min.js')}}">
                                    </script>
                                    <script src="{{asset('vendor/vector-map/jquery.vmap.js')}}"></script>
                                    <script src="{{asset('vendor/vector-map/jquery.vmap.min.js')}}"></script>
                                    <script src="{{asset('vendor/vector-map/jquery.vmap.sampledata.js')}}"></script>
                                    <script src="{{asset('vendor/vector-map/jquery.vmap.world.js')}}"></script>

                                    <!-- Main JS-->
                                    <script src="{{asset('js/main.js')}}"></script>
    <script type="text/javascript">
        function readURLFav(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#favBlah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on("change", ".file_multi_video", function(evt) {
            var $source = $('#video_here');
            $source[0].src = URL.createObjectURL(this.files[0]);
            $source.parent()[0].load();
        });
    </script>
                                    <script>
                                        CKEDITOR.replaceClass('ckeditor');

                                    </script>
                                    <script type="text/javascript">
                                       function readURL(input) {
                                        if (input.files && input.files[0]) {
                                            var reader = new FileReader();

                                            reader.onload = function (e) {
                                                $('#blah')
                                                .attr('src', e.target.result)
                                                .width('50%')
                                                .height('90%');
                                            };

                                            reader.readAsDataURL(input.files[0]);
                                        }
                                    }
                                    $(document).on("change", ".file_multi_video", function(evt) {
                                      var $source = $('#video_here');
                                      $source[0].src = URL.createObjectURL(this.files[0]);
                                      $source.parent()[0].load();
                                  });
                              </script>

                          </body>

                          </html>
<!-- end document-->