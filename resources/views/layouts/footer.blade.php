			<!-- Footer -->

			<footer class="footer">
				<div class="footer_content">
					<div class="container">
						<div class="row">
				
							<div class="col-sm-4 footer_col">
								<div class="footer_about">
									<div class="footer_logo">
										<a href="#">
										<div><a href="/"><img src="{{asset('/images/logo.png')}}"></a></div>
										</a>
									</div>
									<div class="footer_about_text">
										
									</div>
								</div>
							</div>

		<div class="col-sm-8 responsive_footer">
			<div class="row">
							<!-- Footer Contact Info -->
							<div class="col-lg-3 footer_col">
								<div class="footer_location">
									<div class="sqs-block-content"><h3 style="white-space: pre-wrap;">Sitemap</h3>
									<ul class="contact_list">
										<li><p style="white-space: pre-wrap;"><a href="/">Home</a></li>
										<li><p style="white-space: pre-wrap;"><a href="/about-us">About us</a></li>
										<li><p style="white-space: pre-wrap;"><a href="/gallery">Gallery</a></li>
										<nav class="footer_nav ml-md-auto">
										<div class="social ">
							<ul class="d-flex flex-row align-items-center justify-content-start">
								<li><a target="_blank" href="https://www.facebook.com/Haruosi/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="https://www.twitter.com/Haruosi/"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a target="_blank" href="https://www.instagram.com/Haruosi/"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
								<li><a target="_blank" href="https://www.linkedin.com/Haruosi/"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							</ul>
						</div>
									</nav>
									</ul>
									
								</div>
							</div>
						</div>

							<!-- Footer Locations -->
							<div class="col-lg-3 footer_col">
								<div class="footer_location">
									<div class="sqs-block-content"><h3 style="white-space: pre-wrap;">Information</h3>
										<ul class="contact_list">
											<li><p style="white-space: pre-wrap;"><a href="/contact">Contact</a></li>
												<li><p style="white-space: pre-wrap;"><a href="/blogs">Blog</a></li>
													<li><p style="white-space: pre-wrap;"><a href="#">Privacy Policy</a></li>
														<li><p style="white-space: pre-wrap;"><a href="#">Terms &amp; Conditions</a></li>
														</ul>
													</div>
												</div>
											</div>
							<div class="col-lg-6 footer_col">
								<div class="footer_location">
									<div class="sqs-block-content"><h3 style="white-space: pre-wrap;">Mailing list</h3>
									<ul class="contact_list">
										<p style="white-space: pre-wrap;">Join our mailing list to receive 10% off your first D’Luxe gift box.</p>
										<div class="newsletter-form-wrapper newsletter-form-wrapper--layoutFloat newsletter-form-wrapper--alignCenter">
	  <form class="newsletter-form" method="post" action="#">              
	            <div class="newsletter-form-field-wrapper form-item field email required">
	              <label class="newsletter-form-field-label title" for="email">Email Address</label>
	              <input class="newsletter-form-field-element field-element" name="email" type="text" spellcheck="false" placeholder="Email Address" />
				</div>
	      <div class="newsletter-form-button-wrapper">
	        <button class="newsletter-form-button" type="submit">SIGN UP</button>
	      </div>
	  </form>
	</div>
	</div>
	</div>
									</ul>
								</div>
							</div>
						</div>

	</div>
		</div>
					</div>
				</div>
				<div class="footer_bar">
					<div class="container">
						<div class="row">
							<div class="col">
								<div class="footer_bar_content  d-flex flex-md-row flex-column align-items-md-center justify-content-start">
									<div class="copyright"><!-- Link back to nepgeeks can't be removed. Template is licensed under CC BY 3.0. -->
		Copyright &copy;<script type="772b37959b40d44eaa8e7c82-text/javascript">document.write(new Date().getFullYear());</script> All rights reserved
		</div>
									
								</div>
							</div>
						</div>
					</div>			
				</div>
			</footer>



			