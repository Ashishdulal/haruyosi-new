<div class="container">
	<div class="row header_gap">
		<div class="col-sm-4 header_logo">
			 <a href="/"><img src="{{asset('/images/logo.png')}}"></a>
		</div>
		<div class="col-sm-8 menu_header">
<div class="hamburger" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars" aria-hidden="true"></i></div>
			<nav class="navbar navbar-expand-lg navbar-light">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}"><a class="nav-link" href="{{ url('/' )}}">
                Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ Request::segment(1)=='about-us' ? 'active' : '' }}">
              <a class="nav-link" href="/about-us">About us <span class="sr-only">(current)</span></a>
            </li>
<!--             <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Shop
             </a>
             <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">All</a>
              <a class="dropdown-item" href="#">Baby</a>
              <a class="dropdown-item" href="#">Christmas</a>
              <a class="dropdown-item" href="#">Foodie</a>
              <a class="dropdown-item" href="#">For Her</a>
              <a class="dropdown-item" href="#">For Him</a>
            </div>
          </li> -->
          <li class="nav-item {{ Request::segment(1)=='gallery' ? 'active' : '' }}">
              <a class="nav-link" href="/gallery">Gallery <span class="sr-only">(current)</span></a>
            </li>
          <li class="nav-item dropdown {{ Request::segment(1)=='training' ? 'active' : '' }}">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Training
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="training">Waste Management</a>        
              <a class="dropdown-item" href="training">Decoration</a>        
            </div>
          </li>
          <li class="nav-item {{ Request::segment(1)=='blogs' ? 'active' : '' }}">
            <a class="nav-link" href="/blogs">Blog</a>
          </li>
          <li class="nav-item {{ Request::segment(1)=='contact' ? 'active' : '' }}">
            <a class="nav-link" href="/contact">Contact</a>
          </li>
        </ul>
      </div>
    </nav>
		</div>
	</div>
</div>

