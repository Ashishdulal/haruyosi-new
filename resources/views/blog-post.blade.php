 @extends('layouts.new.app', ['title' => 'News Post'],['discription'=> ($pageSetting->tagline)])
 @section('metaDescription')
 <div style="display: none;">
 <meta property="og:url"           content="/news-detail/{{$id}}" />
 <meta property="og:type"          content="website" />
 <meta property="og:title"         content="{{$blogs->title}}" />
 <meta property="og:description"   content="<?php echo ($blogs -> description)?>" />
 <meta property="og:image"         content="/uploads/{{$blogs-> f_image}}" />
 <meta property="og:image:width" content="450"/>
<meta property="og:image:height" content="298"/>
</div>
 @endsection
 @section('content')
 <style type="text/css">
  .gallery-item{
    height: auto;
  }
</style>
<section class="section-page-title" style="background-image: url(/images/banner/{{$blogBanner->blog_banner}}); background-size: cover;">
  <div class="container">
    <h1 class="page-title">{{ $blogs->title }}</h1>
  </div>
</section>
<section class="breadcrumbs-custom">
  <div class="container">
    <ul class="breadcrumbs-custom-path">
      <li><a href="/">Home</a></li>
      <li><a href="/news">News</a></li>
      <li class="active">{{ $blogs->title }}</li>
    </ul>
  </div>
</section>
<section class="section section-lg bg-default">
  <div class="container container-responsive">
    <div class="row row-50">
      <div class="col-lg-8">
        <div class="single-post-content">
          <h4>{{ $blogs->title }}</h4>
          <!--           <iframe src="https://www.facebook.com/plugins/share_button.php?href=http%3A%2F%2Fmysedap.net%2Fblog-detail%2F2&layout=button&size=large&width=77&height=28&appId" width="77" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe> -->
          <ul class="post-meta-list">
            <li class="time">{{ $blogs->created_at->format('d M , Y') }}</li>
            <li>by Admin</li>
          </ul><img src="/uploads/{{$blogs-> f_image}}" alt="{{$blogs->name}}" width="735" height="404"/>
          <div class="blog-prag-all"><?php echo ($blogs -> description)?></div>
          <img src="/uploads/{{$blogs-> i_image}}" alt="{{$blogs->name}}" width="735" height="404"/>
          <!-- <div class="single-post-share-block">
            <p class="big text-gray-800">Share:</p>
            <ul class="list-inline social-list">
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-facebook" href="#"></a></li>
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-twitter" href="#"></a></li>
              <li><a class="icon icon-circle icon-circle-sm icon-circle-gray fa-google-plus" href="#"></a></li>
            </ul>
          </div> -->
          
        </div>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <!-- Your share button code -->
        <div class="fb-share-button" 
        data-href="http://mysedap.net/news-detail/{{$id}}" 
        data-layout="button_count">
      </div>

        <!-- <div class="single-post-author">
          <div class="single-post-author-img"><img src="/images/user-5-115x115.jpg" alt="" width="115" height="115"/>
            <h5>Mary Lucas </h5>
          </div>
          <div class="single-post-author-content">
            <h5>About the Author</h5>
            <p>Donec posuere pellentesque accumsan. Quisque cursus in ex id mollis. Suspendisse potenti. Praesent fermentum fringilla erat ac porta. Praesent congue justo efficitur libero dapibus, et eleifend tellus. Fermentum fringilla erat ac porta. Praesent congue justo efficitur.</p>
          </div>
        </div> -->
              <!-- <div class="post-comment-block">
                <h4>Comments</h4>
                <div class="post-comment-item">
                  <div class="post-comment-item-img"><img src="images/user-4-90x90.jpg" alt="" width="90" height="90"/>
                  </div>
                  <div class="post-comment-item-content">
                    <div class="post-comment-item-header">
                      <ul class="list-inline">
                        <li><a href="#">Janice Brown</a></li>
                        <li>June 20, 2018 at 10:15am</li>
                      </ul>
                    </div>
                    <div class="post-comment-item-text">
                      <p>Thanks to the author for such a useful article. It is really important for me to be informed about beauty news.</p>
                    </div>
                    <div class="post-comment-item-footer"><a href="#"><span class="icon mdi mdi-reply"> </span>Reply</a></div>
                  </div>
                </div>
                <div class="post-comment-item post-comment-item-reply">
                  <div class="post-comment-item-img"><img src="images/user-3-90x90.jpg" alt="" width="90" height="90"/>
                  </div>
                  <div class="post-comment-item-content">
                    <div class="post-comment-item-header">
                      <ul class="list-inline">
                        <li><a href="#">Betty Riley</a></li>
                        <li>June 20, 2018 at 10:30am</li>
                      </ul>
                    </div>
                    <div class="post-comment-item-text">
                      <p>Thank you!</p>
                    </div>
                    <div class="post-comment-item-footer"><a href="#"><span class="icon mdi mdi-reply"></span>Reply</a></div>
                  </div>
                </div>
              </div> -->
<!--               <div class="form-comment">
                <h4>Leave a Comment</h4>
                <form class="rd-mailform text-left rd-form" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
                  <div class="row row-15">
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-name">First name</label>
                        <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-sec-name">Last name</label>
                        <input class="form-input" id="contact-sec-name" type="text" name="sec-name" data-constraints="@Required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-phone">Phone</label>
                        <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@Numeric @Required">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-email">E-Mail</label>
                        <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Email @Required">
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-wrap">
                        <label class="form-label" for="contact-message">Message</label>
                        <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-button group-sm text-left">
                    <button class="button button-primary" type="submit">Submit</button>
                  </div>
                </form>
              </div> -->
            </div>
             <div class="col-lg-4">
              <div class="blog-aside-list">
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Categories</h4>
                  <ul class="list-marked">
                    <li><a href="/news">All</a></li>
                    @foreach($blogcategories as $blogcategory)
                    @if ($blogcategory->id !== 3)
                    <li><a href="/news/category/{{$blogcategory->id}}">{{$blogcategory->title}}</a></li>
                    @endif
                    @endforeach
                  </ul>
                </div>
            <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Portfolio</h4>
                  <ul class="list-marked">
                    <?php $count=1; ?>
                    @foreach($gallery as $galley)
                    <li><a target="_blank" href="{{$galley->url}}">{{$galley->name}}</a></li>
                    <?php $count ++; ?>
                    <?php if($count > 4)
                    break
                    ?>
                    @endforeach
                  </ul>
                </div>
<!--                 <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Search</h4>
                  <form class="rd-search" action="search-results" method="GET" data-search-live="rd-search-results-live">
                    <div class="form-wrap">
                      <label class="form-label" for="rd-search-form-input">Search...</label>
                      <input class="form-input" id="rd-search-form-input" type="text" name="s" autocomplete="off">
                      <button class="icon mdi mdi-magnify" type="submit"></button>
                    </div>
                  </form>
                </div> -->
               <!--  <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Archive</h4>
                  <ul class="list-marked list-two-column">
                    <li><a href="#">Jun 2018</a></li>
                    <li><a href="#">Jul 2018</a></li>
                    <li><a href="#">Aug 2018</a></li>
                    <li><a href="#">Sep 2018</a></li>
                    <li><a href="#">Oct 2018</a></li>
                    <li><a href="#">Nov 2018</a></li>
                    <li><a href="#">Dec 2018</a></li>
                    <li><a href="#">Jan 2018</a></li>
                    <li><a href="#">Feb 2018</a></li>
                    <li><a href="#">Mar 2018</a></li>
                  </ul>
                </div> -->
                <div class="blog-aside-list-item">
                  <h4 class="blog-aside-list-item-title">Recent News</h4>
                  <?php $count=1; ?>
                  @if(count($allblogs))
                  @foreach($allblogs as $blog)
                  @if ($blog->cat_id != 3)
                  <div class="blog-aside-post">
                    <h5><a href="/news-detail/{{$blog->id}}">{{$blog->title}}</a></h5>
                    <p>{{ $blog->created_at->format('d M , Y | H:i') }}</p>
                  </div>
                  <?php $count ++; ?>
                  <?php if($count > 4)
                  break
                  ?>
                  @endif
                  @endforeach
                  @else
                  <h5>There are no posts in News.</h5>
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Page Footer-->
      @endsection