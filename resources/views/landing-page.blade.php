 @extends('layouts.new.app', ['title' => 'Landing Page'],['discription'=> ($pageSetting->tagline)])
 @section('metaDescription')
 <meta name="tagline" content="{{$pageSetting->tagline}}">
 <meta name="description" content="{{$pageSetting->meta_description_seo}}">
 <meta name="site url" content="{{$pageSetting->site_url}}">
 <meta name="keywords" content="{{$pageSetting->meta_keywords_seo}}">
 @endsection
@section('customCss')
<style type="text/css">
  .rd-navbar-aside-outer.rd-navbar-collapse.toggle-original-elements {
    display: none;
}
header.section.page-header {
     height: auto !important; 
}
.social-block.my-footer-link{
  display: none;
}
</style>
@endsection
 @section('content')

 <section class="section swiper-container swiper-slider swiper-slider-2 slider-scale-effect" data-loop="false" data-autoplay="5500" data-simulate-touch="false" data-slide-effect="fade">
 	<div class="swiper-wrapper">
 		@foreach($sliders as $slider)
 		<div class="swiper-slide">
 			<div class="slide-bg" style="background-image: url(&quot;/uploads/{{$slider->image}}&quot;)"></div>
 			<div class="swiper-slide-caption section-md">
 				<div class="container">
 					<div class="row">
 						<div class="col-sm-10 col-lg-7 col-xl-6 swiper-caption-inner">
 							<h1 data-caption-animate="fadeInUp" data-caption-delay="100"><span class="text-primary">{{$slider->title}}</span><br>{{$slider->title_1}}
 							</h1>
 							<div class="divider-lg" data-caption-animate="fadeInLeft" data-caption-delay="550"></div>
 							<p class="lead" data-caption-animate="fadeInUp" data-caption-delay="250"><?php echo ($slider->description)?></p>
 							<a class="button button-default-outline" data-toggle="modal" data-target="#myModal" href="#" data-caption-animate="fadeInUp" data-caption-delay="450">Book Now</a>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>
 		@endforeach

 	</div>
 	<!-- Swiper Pagination -->
 	<div class="swiper-pagination"></div>
 	<div class="swiper-button-prev"></div>
 	<div class="swiper-button-next"></div>
 </section>

 <section class="section section-md">
 	<div class="container">
 		<div class="row row-50">
 			<div class="col-lg-6">
 				<h2>Signup</h2>
 				<div class="divider-lg"></div>
 				<?php echo ($contact->banner_text)?>
 				@if (count($errors) > 0)
 				<div class="alert alert-danger">
 					<button type="button" class="close" data-dismiss="alert">×</button>
 					<ul>
 						@foreach ($errors->all() as $error)
 						<li>{{ $error }}<br></li>
 						@endforeach
 					</ul>
 				</div>
 				@endif
 				@if ($message = Session::get('success'))
 				<div class="alert alert-success alert-block">
 					<button type="button" class="close" data-dismiss="alert">×</button>
 					<strong>{{ $message }}</strong>
 				</div>
 				@endif
 				<!-- RD Mailform-->
 				<form class="text-left" action="{{url('sendemail/send')}}" method="post" >
 					@csrf
          <input type="hidden" name="subject" value="New Register/Landing Page">
          <input type="hidden" name="message" value="Get The new member Registered">
 					<div class="row row-15">
 						<div class="col-sm-6">
 							<div class="form-wrap">
 								<input class="form-input" id="contact-name" type="text" name="name" placeholder="First name" required="">
 							</div>
 						</div>
 						<div class="col-sm-6">
 							<div class="form-wrap">
 								<input class="form-input" id="contact-sec-name" type="text" name="sec_name" placeholder="Last name" required="">
 							</div>
 						</div>
 						<div class="col-sm-12">
 							<div class="form-wrap">
 								<input class="form-input" id="contact-phone" type="text" placeholder="Phone" name="phone" required="">
 							</div>
 						</div>
 						<div class="col-sm-12">
 							<div class="form-wrap">
 								<input class="form-input" id="contact-email" type="email" name="email" placeholder="E-Mail" required="">
 							</div>
 						</div>
 					</div>
 					<div class="form-button group-sm text-left">
 						<button class="button button-primary" type="submit">Register</button>
 					</div>
 				</form>
 			</div>
 			<div class="col-lg-6">
 				<div class="box-images box-images-variant-3">
          <div class="box-images-item" data-parallax-scroll="{&quot;y&quot;: -20,   &quot;smoothness&quot;: 30 }"><img src="/uploads/about-us/{{$about->who_are_we_image1}}" alt="" width="470" height="282"/>
          </div>
          <div class="box-images-item box-images-without-border" data-parallax-scroll="{&quot;y&quot;: 40,  &quot;smoothness&quot;: 30 }"><img src="/uploads/about-us/{{$about->who_are_we_image2}}" alt="" width="470" height="282"/>
          </div>
        </div>
 			</div>
 		</div>
 	</div>
 </section>
 <section style="padding-top: 0 !important;" id="haruyosi_about" class="section section-md bg-default text-center">
  <div class="container">
    <h2>Say Hello To Full Service Plan</h2>
    <div class="divider-lg"></div>
    <p class="block-lg">By doing it all we keep fees down ald let you focus on whats most important.</p>
    <div class="row row-30">
      <div class="col-12">
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-creative" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          <div class="team-minimal team-minimal-with-shadow">
            <figure style="background-color: #f6f6f6;border-radius: 50%;"><img src="https://img.icons8.com/cotton/64/000000/hand-right.png"></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title">Less Work For You</h4>
            </div>
              <p>We Integrate with your payroll and handle all the reporting, compilance, and investments.</p>
          </div>
          @endforeach

        </div>
      </div>
      <div class="col-12"><a class="button button-default-outline" data-toggle="modal" data-target="#myModal" href="#">Schedule a demo</a></div>
      <!-- <div class="col-12"><a class="button button-default-outline" href="/our-team">View all team</a></div> -->
    </div>
  </div>
</section>
<section class="section bg-default">
  <div class="container">
    <div class="row row-50">
      <div class="col-sm-12 col-lg-12">
        <p class="text-center text-lg-right"><a class="button-link button-link-icon" href="/about-us#haruyosi_about">View All Team <span class="icon fa-arrow-right icon-primary"></span></a></p>
        <!-- Owl Carousel-->
        <div class="owl-carousel carousel-inset" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
          @foreach($staffs as $staff)
          <div class="team-minimal team-minimal-type-2">
            <figure><img src="/uploads/{{$staff->image}}" alt="" width="370" height="370"></figure>
            <div class="team-minimal-caption">
              <h4 class="team-title"><a href="/about-us#haruyosi_about">{{$staff->name}}</a></h4>
              <p>{{$staff->designation}}</p>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
 <section class="section parallax-container" data-parallax-img="images/parallax-7-1920x1020.jpg">
          <div class="parallax-content section-lg text-center ">
            <div class="container"> 
              <h2>Testimonials</h2>
              <div class="divider-lg"></div>
              <!-- Owl Carousel-->
              <div class="owl-carousel" data-items="1" data-lg-items="3" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false">
                @foreach($testimonials as $testimonial)
                <div class="quote-corporate quote-corporate-center-img">
                  <div class="quote-header">
                    <h4>{{$testimonial->name}}</h4>
                    <p class="big">Client</p>
                  </div>
                  <div class="quote-body">
                    <div class="quote-text">
                      <p><?php echo ($testimonial->description)?></p>
                    </div>
                    <svg class="quote-body-mark" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="66px" height="49px" viewbox="0 0 66 49" enable-background="new 0 0 66 49" xml:space="preserve">
                      <g></g>
                      <path d="M36.903,49v-3.098c9.203-5.315,14.885-12.055,17.042-20.222c-2.335,1.524-4.459,2.288-6.37,2.288                      c-3.186,0-5.875-1.29-8.071-3.876c-2.194-2.583-3.293-5.74-3.293-9.479c0-4.133,1.443-7.605,4.327-10.407                       C43.425,1.405,46.973,0,51.185,0c4.213,0,7.735,1.784,10.566,5.352C64.585,8.919,66,13.359,66,18.669                       c0,7.482-2.85,14.183-8.549,20.112C51.751,44.706,44.902,48.112,36.903,49z M0.69,49v-3.098                        c9.205-5.315,14.887-12.055,17.044-20.222c-2.335,1.524-4.478,2.288-6.423,2.288c-3.152,0-5.823-1.29-8.02-3.876                        C1.096,21.51,0,18.353,0,14.614c0-4.133,1.434-7.605,4.301-10.407C7.168,1.405,10.709,0,14.92,0c4.247,0,7.778,1.784,10.592,5.352                       c2.814,3.567,4.223,8.007,4.223,13.317c0,7.482-2.843,14.183-8.524,20.112C15.53,44.706,8.69,48.112,0.69,49z"></path>
                    </svg>
                  </div>
                  <div class="quote-image"><img src="/uploads/{{$testimonial->image}}" alt="" width="90" height="90"/>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </section>

 @endsection