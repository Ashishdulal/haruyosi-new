<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('service_banner');
            $table->text('portfolio_banner');
            $table->text('blog_banner');
            $table->text('jobs_banner');
            $table->longText('jobs_detail');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs_details');
    }
}
