<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = new User();
        $users->name = 'admin';
        $users->email = 'admin@admin.com';
        $users->image = 'admin-image.png';
        $users->password = bcrypt('admin');
        $users->save();
    }
}
