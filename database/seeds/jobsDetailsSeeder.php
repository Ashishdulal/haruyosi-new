<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Jobs_detail;


class jobsDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobs = new Jobs_detail();
        $jobs->service_banner = 'default-thumbnail.png';
        $jobs->portfolio_banner = 'default-thumbnail.png';
        $jobs->blog_banner = 'default-thumbnail.png';
        $jobs->jobs_banner = 'default-thumbnail.png';
        $jobs->jobs_detail = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $jobs->save();
    }
}
