<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\homePage;

class homePageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $homepages = new homePage();
        $homepages->logo_image = 'default-thumbnail.png';
        $homepages->phone_number = '1234567890';
        $homepages->opening_hours = '10-06';
        $homepages->Social_icon_fb = 'https://www.facebook.com/admin/';
        $homepages->Social_icon_insta = 'https://www.instagram.com/admin/';
        $homepages->Social_icon_twitter = 'https://www.twitter.com/admin/';
        $homepages->Social_icon_linkedin = 'https://www.linkedin.com/admin/';
        $homepages->upper_body_image1 = 'default-thumbnail.png';
        $homepages->upper_body_image2 = 'default-thumbnail.png';
        $homepages->upper_body_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->service_body_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->staff_body_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->contact_body_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->lower_body_image1 = 'motion-video2.mkv';
        $homepages->lower_body_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->schedule_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->why_us_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $homepages->portfolio_content = 'Lorem ipsum dolor sit amet';
        $homepages->save();
    }
}