<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\ContactDetails;

class ContactDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact = new ContactDetails();
        $contact->contact_banner = 'default-thumbnail.png';
        $contact->banner_text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
        $contact->address = 'Washington, USA 6036 Richmond hwy., Alexandria, VA, 2230';
        $contact->phone = '+977-1234567890';
        $contact->mail = 'abc@gmail.com';
        $contact->opening_hour = 'Mon-Fri: 9 am – 6 pm';
        $contact->map = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d56516.27689223948!2d85.29111336131282!3d27.709031933215165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb198a307baabf%3A0xb5137c1bf18db1ea!2sKathmandu%2044600!5e0!3m2!1sen!2snp!4v1577431201166!5m2!1sen!2snp" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>';
        $contact->save();
    }
}
