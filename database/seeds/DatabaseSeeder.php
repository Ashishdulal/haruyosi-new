<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(homePageSeeder::class);
        $this->call(aboutUsSeeder::class);
        $this->call(jobsDetailsSeeder::class);
        $this->call(ContactDetailsSeeder::class);
        $this->call(PageSetttingsSeeder::class);
    }
}
